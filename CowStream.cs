﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matroska
{
	/// <summary>
	/// Implements a <see cref="Memory{T}"/>-based stream that copies its memory on first write (Copy On Write).
	/// </summary>
	internal sealed class CowStream : Stream
	{
		#region Fields

		private Memory<byte> data;
		private bool ownsData;
		private int position;

		#endregion

		#region Properties

		public override bool CanRead => true;
		public override bool CanSeek => true;
		public override bool CanWrite => true;
		public override long Length => this.data.Length;

		public override long Position
		{
			get => this.position;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value));

				if (value > this.Length)
					throw new ArgumentOutOfRangeException(nameof(value));

				this.position = (int)value;
			}
		}
		
		#endregion

		#region Constructors

		public CowStream(Memory<byte> data, bool ownsData = true)
		{
			this.data = data;
			this.ownsData = ownsData;
		}

		public CowStream(CowStream baseStream)
		{
			this.data = baseStream.data;
			this.ownsData = false;
		}

		#endregion

		#region Methods

		#endregion

		public override void Flush()
		{
			
		}

		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}
		
		public override int Read(Span<byte> buffer)
		{
			if (this.position < 0)
				throw new ObjectDisposedException(nameof(CowStream));

			int n = Math.Min(this.data.Length - this.position, buffer.Length);
			this.data.Span.Slice(this.position, n).CopyTo(buffer.Slice(0, n));
			this.position += n;
			return n;
		}
		
		public override int Read(byte[] buffer, int offset, int count) => this.Read(new Span<byte>(buffer, offset, count));

		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken) => this.ReadAsync(new Memory<byte>(buffer, offset, count), cancellationToken).AsTask();

		public override ValueTask<int> ReadAsync(Memory<byte> buffer, CancellationToken cancellationToken = new CancellationToken()) => new ValueTask<int>(this.Read(buffer.Span));

		public override int ReadByte()
		{
			Span<byte> temp = stackalloc byte[1];
			int n = this.Read(temp);
			if (n == 0)
				return -1;
			return temp[0];
		}

		public override void Write(ReadOnlySpan<byte> buffer)
		{
			if (this.position < 0)
				throw new ObjectDisposedException(nameof(CowStream));

			if (this.position + buffer.Length > this.Length)
				throw new NotSupportedException("Can't write past the end of the stream.");

			if (!this.ownsData)
			{
				byte[] dataBuf = new byte[this.data.Length];
				this.data.CopyTo(dataBuf);
				this.data = dataBuf;
				this.ownsData = true;
			}

			buffer.CopyTo(this.data.Span.Slice(this.position, buffer.Length));
			this.position += buffer.Length;
		}

		public override void Write(byte[] buffer, int offset, int count) => this.Write(new ReadOnlySpan<byte>(buffer, offset, count));

		public override ValueTask WriteAsync(ReadOnlyMemory<byte> buffer, CancellationToken cancellationToken = new CancellationToken())
		{
			this.Write(buffer.Span);
			return default;
		}

		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			this.Write(new ReadOnlySpan<byte>(buffer, offset, count));
			return Task.CompletedTask;
		}

		public override void WriteByte(byte value)
		{
			Span<byte> temp = stackalloc byte[1];
			temp[0] = value;
			this.Write(temp);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			if (this.position < 0)
				throw new ObjectDisposedException(nameof(CowStream));
			
			switch (origin)
			{
				case SeekOrigin.Begin:
					this.Position = offset;
					break;
				case SeekOrigin.Current:
					this.Position += offset;
					break;
				case SeekOrigin.End:
					this.Position = this.Length - offset;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(origin), origin, null);
			}

			return this.Position;
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		protected override void Dispose(bool disposing)
		{
			this.data = default;
			this.position = -1;
		}

		public override ValueTask DisposeAsync()
		{
			this.data = default;
			this.position = -1;
			return default;
		}
	}
}
