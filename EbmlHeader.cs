﻿namespace Matroska
{
	/// <summary>
	/// Represents the header of an EBML element.
	/// </summary>
	public readonly struct EbmlHeader
	{
		/// <summary>
		/// The ID of the element.
		/// </summary>
		public readonly uint ID;

		/// <summary>
		/// The length of the element.
		/// </summary>
		public readonly long Length;

		public EbmlHeader(uint id, long length)
		{
			this.ID = id;
			this.Length = length;
		}

		public override string ToString()
		{
			return $"0x{this.ID:X8}, {this.Length} bytes";
		}
	}
}